import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RecordsService } from '../services/records.service';
import { RuleService } from '../services/rule.service';
import { first } from 'rxjs/operators'

const STATUS_NOT_STARTED: number = 0;
const STATUS_ONGOING = 1;
const STATUS_LOST = 2;
const STATUS_WON = 3;
const STATUS_LOGGED = 4;
const NOTIFICATION_MESSAGES: string[] = ['','Illegal move',`Game ended!`,'You made it all the way, congrats!'];

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})

export class BoardComponent implements OnInit {


  dataSet: number[] = [];
  cols: number[] = [];
  rows: number[] = [];
  nextStep = 1;
  showNotification: boolean = false;
  availableMoves: number[] = [];
  notificationMessage: string = '';
  lastClick = 0;
  timerRunning: boolean = false;
  helpEnabled: boolean = false;
  helpButtonText = 'Show';
  level: number = 0;
  endTime: string = '00:00:00';
  gameStatus: number = STATUS_NOT_STARTED;
  resetTime: boolean = false;


  constructor(private ruleService: RuleService, private recordsService: RecordsService, private activatedroute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedroute.data.subscribe(data => {
      this.level = data.level;
    })
    this.cols = new Array(this.level > 20 ? 10 : 5);
    this.rows = this.loop(this.level > 20 ? this.level / 10 : 4);
    this.dataSet = this.loop(this.level);

  }

  loop(size: number) {
    let array = [0];
    for (let i = 1; i < size; i++) {
      array.push(0);
    }
    return array;
  }



  click(event: any) {
    if (this.gameStatus > STATUS_ONGOING) return
    if (!this.timerRunning) {
      this.toggleTimer(1);
    };

    let click = parseInt(event.target.id);

    if (this.nextStep != 1
      && !this.ruleService.isAllowed(click, this.lastClick, this.dataSet, this.cols.length)) {
      console.log(this.setNotification());
      this.setNotification();
      return;
    }

    this.showNotification = false;
    event.target.textContent = this.nextStep;

    //won game
    if (this.nextStep === this.level) {
      this.toggleTimer(STATUS_WON);
      return;
    }

    this.dataSet[click] = this.nextStep;
    this.nextStep++;
    this.lastClick = click;
    if (this.helpEnabled) this.setHelpColor(false);

    this.availableMoves = this.ruleService.movesAvailable(click, this.dataSet, this.cols.length);

    //lost game
    if (this.availableMoves.length === 0) {
      this.toggleTimer(STATUS_LOST);
      return;
    }

    if (this.helpEnabled) this.setHelpColor(true);

  }

  setHelpColor(enable:boolean) {
    let color = enable ? 'green' : 'white'
    this.availableMoves.forEach(move => {
      let element = document.getElementById(`${move}`);
      if (element) {
        element.style.backgroundColor = color;
      }
    });
  }

  toggleHelp() {
    this.helpEnabled = !this.helpEnabled;
    this.helpButtonText = this.helpEnabled ? 'Hide' : 'Show';
    this.setHelpColor(this.helpEnabled);
  }


  gameFinished() {
    this.setNotification();
    this.recordsService.getRank(this.level, this.nextStep, this.endTime).pipe(first()).subscribe(rank => {
      console.log(`Congratulations, you made it to number ${rank} on the high scores`);
      if (rank <= 5) {
        let input = prompt('Username for high score list');
        let ok = this.recordsService.setRecord(input ? input : 'anon', this.level, this.nextStep, this.endTime)
        console.log(`Record saved ${ok}`)
      }
    })

    this.gameStatus = STATUS_LOGGED
  }

  timerStopped(stopTime: string) {
    this.showNotification = false;
    if (this.gameStatus < STATUS_LOST || this.gameStatus === STATUS_LOGGED) {
      console.log(stopTime);
      return
    }
    this.endTime = stopTime;
    this.gameFinished();
  }

  toggleTimer(status: number) {
    if(status === STATUS_LOST) this.nextStep--;
    this.gameStatus = status;
    this.timerRunning = !this.timerRunning
  }

  setNotification() {
    this.notificationMessage = NOTIFICATION_MESSAGES[this.gameStatus];
    this.showNotification = true;
  }

  resetBoard() {
    if (this.timerRunning) this.toggleTimer(STATUS_NOT_STARTED);
    this.gameStatus = 0; //also resets timer
    this.showNotification = false;
    this.dataSet = this.loop(this.level);
    this.nextStep = 1;
    this.setHelpColor(false);
    this.availableMoves = [];
    let boxes =  document.getElementsByClassName('board-box');
    Array.from(boxes).forEach(box => {
      box.textContent = '0';
    })
  }
}