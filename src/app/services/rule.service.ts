import { Injectable } from '@angular/core';


const rowOffset = 3;
const columnOffset = 3;
const cornerOffset = 2;


@Injectable({
  providedIn: 'root'
})
export class RuleService {

  constructor( ) { }

  isAllowed(click: number, lastClick: number, dataSet: number[], columns: number) {
    //console.log('here ',click);

    if (click < 0 
        || click > dataSet.length-1
        || dataSet[click] != 0){
      return false;
  }

  let row = Math.floor(click/columns);
  let column = click % columns;
  let lastRow = Math.floor(lastClick/columns);
  let lastColumn = lastClick % columns;



  if(  (row === lastRow                      && (column === (lastColumn + columnOffset) || column === (lastColumn - columnOffset))) //same row, east & west
    || (column === lastColumn                && (row === (lastRow + rowOffset) || row === (lastRow - rowOffset))) // same column, south & north
    || (column === lastColumn - cornerOffset && (row === (lastRow - cornerOffset) || row === (lastRow + cornerOffset))) // west corners, northwest & southwest
    || (column === lastColumn + cornerOffset && (row === (lastRow - cornerOffset) || row === (lastRow + cornerOffset))) // east corners, northeast & southeast
    ) return true; 
  
  return false;

  }

  movesAvailable(click: number, dataSet: number[], columns: number) {
    let west = click - rowOffset;
    let east = click + rowOffset;
    let north = click - columnOffset*columns;
    let south = click + columnOffset*columns;
    let southwest = click - (cornerOffset*columns) - cornerOffset;
    let southeast = click - (cornerOffset*columns) + cornerOffset;
    let northwest = click + (cornerOffset*columns) - cornerOffset;
    let northeast = click + (cornerOffset*columns) + cornerOffset;

    let moves = [west, east, north, south, southwest, southeast, northwest, northeast];
    let movenames = ['west', 'east', 'north', 'south', 'southwest', 'southeast', 'northwest', 'northeast'];   //for debugging

    let availableMoves: number[] = [];

    for (let i = 0; i < moves.length; i++){
      if(this.isAllowed(moves[i], click, dataSet, columns)) {
        availableMoves.push(moves[i]);
        console.log(`${movenames[i]} ${moves[i]}  OK`) //logs for debugging
      } else {
        console.log(`${movenames[i]} ${moves[i]} NOK`)
      }
    }
    return availableMoves;
  }
}

