import { Component, Input } from '@angular/core';
import { RecordsService } from '../services/records.service';

@Component({
  selector: 'app-score-table',
  templateUrl: './score-table.component.html',
  styleUrls: ['./score-table.component.scss']
})
export class ScoreTableComponent {
  records$: any;

  @Input() level: number = 0;

  constructor(private recordsService: RecordsService) { 
    this.records$ = this.recordsService.getRecords(this.level)
  }

  ngOnChanges() {
    this.records$ = this.recordsService.getRecords(this.level)
  }

}
