import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BoardComponent } from './board/board.component';

const routes: Routes = [
  {path: 'play20', component: BoardComponent, data :{ level:20}},
  {path: 'play50', component: BoardComponent, data :{ level:50}},
  {path: 'play100', component: BoardComponent, data :{ level:100}},
  {path: 'play150', component: BoardComponent, data :{ level:150}},
  {path: 'play200', component: BoardComponent, data :{ level:200}},
  {path: 'play500', component: BoardComponent, data :{ level:500}}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
