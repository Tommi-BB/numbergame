import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFireDatabase } from '@angular/fire/database';


@Injectable({
  providedIn: 'root'
})
export class RecordsService {
  
 

  /* records100$: Observable<any>;
  records50$: Observable<any>;
  records20$: Observable<any>; */

  constructor(private db: AngularFireDatabase) {
    /* this.records100$ = this.db.list('webRecords/records100').valueChanges();
    this.records50$ = this.db.list('webRecords/records50').valueChanges();
    this.records20$ = this.db.list('webRecords/records20').valueChanges(); */
   }

  getRecords(level: number): Observable<any> {

    let records = this.getRecordsForLevel(level)    
    return this.sortRecords(records)

  }

  //returns top5
  sortRecords(records: Observable<any>): Observable<any> {
    return records.pipe(map(data => {
      data.sort((a: any,b: any) => {
        if(a.score != b.score) return a.score > b.score ? -1 : 1
        else return a.time < b.time ? -1 : 1
      })
      return data.slice(0,5)
    }));
  }

  getRank(level: number, nextInt: number, endTime: string) {
    let records = this.getRecordsForLevel(level)
    records = this.sortRecords(records)
    
    return records.pipe(map((data:any) => {
      for (let i = 0; i < 5; i++) {
        try {
          if (data[i]?.score < nextInt) {
            console.log(`first: round ${i}, score ${data[0].score}, time ${data[0].time}`);
            return i + 1;
          }
          if (data[i]?.score === nextInt && this.timeInMS(data[i]?.time) > this.timeInMS(endTime)) {
            console.log(`second: round ${i}, score ${data[0].score}, time ${data[0].time}`);
            return i + 1;
          }
        } catch (e) {
          console.log(e);
        }
      }
      return data?.length < 5 ? data.length : 42;
    }))
  }

  timeInMS(time: string) {
    let timeParts = time.split(':')
    let timeMS
    if(timeParts.length === 3) {
      timeMS = parseInt(timeParts[0])*60000*60 + parseInt(timeParts[1])*60000 + parseInt(timeParts[2])
    } else {
      timeMS = parseInt(timeParts[0])*60000*60 + parseInt(timeParts[1])*60000
    }
    return timeMS;
  }

  getRecordsForLevel(level: number): Observable<any> {
    let listName = `webRecords/records${level}`;
    return this.db.list(listName).valueChanges();
  }

  setRecord(name: string, level: number, nextInt: number, endTime: string) {
    let listName = `webRecords/records${level}`;
    let ref;
    try {
      ref = this.db.list(listName).push({name:name,score:nextInt,time:endTime,timestamp:Date.now()});
    } catch (error) {
      console.log(error)
      return false;
    }
    
    return ref;
  }
}