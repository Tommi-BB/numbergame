import { Component, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss']
})
export class TimerComponent {

  
  counter: number = 0;
  timerRef: any;
  milliseconds: string = '00';
  seconds: string = '00';
  minutes: string = '00';
  
  @Input() timerRunning: boolean = false;
  @Input() gameStatus: number = 0;
  @Output() endTimeEvent = new EventEmitter<string>();

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if(changes['timerRunning']?.currentValue) this.startTimer(); 
    else {
      clearInterval(this.timerRef)
      this.endTimeEvent.emit(`${this.minutes}:${this.seconds}:${this.milliseconds}`)
    } 
    if (changes['gameStatus']?.currentValue === 0) {
      console.log('timer reset');
      this.counter = 0;
      this.milliseconds = '00';
      this.seconds = '00';
      this.minutes = '00';
    }

  }

  startTimer() {
    const startTime = Date.now() - (this.counter || 0);
      this.timerRef = setInterval(() => {
        this.counter = Date.now() - startTime;
        this.milliseconds = Math.floor(Math.floor(this.counter % 1000) / 10).toFixed(0);
        this.minutes = `${Math.floor(this.counter / 60000)}`;
        this.seconds = Math.floor(Math.floor(this.counter % 60000) / 1000).toFixed(0);
        if (Number(this.minutes) < 10) {
          this.minutes = '0' + this.minutes;
        } else {
          this.minutes = '' + this.minutes;
        }
        if (Number(this.milliseconds) < 10) {
          this.milliseconds = '0' + this.milliseconds;
        } else {
          this.milliseconds = '' + this.milliseconds;
        }
        if (Number(this.seconds) < 10) {
          this.seconds = '0' + this.seconds;
        } else {
          this.seconds = '' + this.seconds;
        }
    })
  }

}
